# Exercise 5: Playing with History

## Part 1: The Commit Log

Git, like any other version control system, allows us to easily view the commit log.

1. Run the `git log` command. Depending on the OS, git may open the log in a pager. 

The arrow keys can be used to scroll; the `/` can be used to search, for example: `/trainees` will highlight all instances of the word `trainees` in the log. Using the `/` command again without parameters will scroll to the next occurence. The `q` key may be used to quit.

2. The git log can be customized with command-line options. Aliases are useful for remembering these. Try running `git log --oneline --decorate --graph` and observe the difference.

3. The log can be further customized with the `pretty` argument. Try `git log --graph --pretty='''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'`. See the git log manual for more information (`git help log`)


## Part 2: Rewriting History

It may sometimes happen that many commits are made in order to test something before arriving at a final result. We may not always want to commit the entire history.

1. Let's simulate a few intermediate changes. First create a new branch called `feature/modify-history` and check it out.

2. We need to create a few commits. First modify Adam's skill level in the trainees file to `4`. Commit this change.

3. Next, modify Adam's project to `PRJ2` and commit the change.

4. Finally, modify Aurelien's project to `PRJ3` and commit the change as well.

5. Run the `git log` command. We should now be 3 commits ahead of master. 

6. Before updating, let's combine the first 2 commits. Run the `git rebase -i HEAD~3` command. The `-i` flag specifies an interactive rebase, and the `HEAD~3` parameter means look back 3 commits from the HEAD.

---

The interactive rebase command will open a text editor. In the comments, the different commands can be found:

| Command | Description     |
| ------- | --------------- |
| pick    | Use this commit |
| reword  | Rewrite the commit message |
| edit    | Stop for amending | 
| squash  | Merge this commit into the previous one, rewriting the commit message |
| fixup   | Merge this commit into the previous one, keeping the previous message |
| exec    | Execute a command in the shell. The command is assumed to be the rest of the line |
| break   | Stop the rebase at this point. Continue with `git rebase --continue` |
| drop    | Remove the commit |

There are others but they are rarely used.

**N.B.** Commits are ordered from the oldest on top to the newest on the bottom!

---

7. We want to squash the first two commits together, so in this order we'll use `pick`, `squash`, `pick` for our three commits. For example:

```
pick 6688b8b Update Adam's skill
squash f790a12 Update Adam's project
pick 134ac7a Update Aurelien's project
```

Save and close the file to activate the rebase.

8. Edit the commit message to be more coherent.

9. Use the `git log` command once more. Notice how we only have 2 commits on our branch.

---
**/!\ WARNING /!\\**  Only rebase commits which have **NOT** already been pushed to the remote repository. Bad things may happen otherwise. DO NOT _EVER_ USE THE REBASE TOOL ON THE MAIN DEVELOPMENT BRANCH.
---
