# Exercise 4: Gitlab

## Part 1: Setting Up

1. First login to <https://gitlab.com> using your gitlab credentials

2. Go to your account settings: ![Top Right, Click User Avatar..Settings](img/gitlab-settings.png)

3. Go to `SSH Keys`

4. Generate an SSH Key if you don't already have one:

   a. In the Git Bash terminal, type `ssh-keygen -t rsa -b 4096 -C "your.email@example.com"` (use a real address to identify the key)

   b. Set the path for the ssh keys, the default is usually good.

   c. You will be prompted to input a password for the private key. While this is recommended, you will be asked for your password every time you wish to push or pull from the gitlab repository.

5. Associate the SSH Key to your gitlab account:

   a. Copy the **Public** key to the clipboard: `cat ~/.ssh/id_rsa.pub | clip` (Assuming the default path)

   b. Paste the key into the large text box on the account settings page labelled `Key`

   c. Click on `Add Key`

6. Check that everything is ok by attempting to connect to the server using the following command: `ssh -T git@gitlab.com`

**N.B.** On the first connection, you may be asked to verify the key fingerprint of the server. Answer `yes` to add the server to the list of trusted hosts.

## Part 2: Creating a new Project

1. To create a new project, we click the `+` button in the top bar. ![Plus..New Project](img/new-project.png)

2. Enter `Gitlab Training` appended by your name as the name for your project (eg `Gitlab Training Adam`), and set its visibility to `Internal`

3. Leave _Initialize repository with a README_ unchecked for now.

4. Click on `Create project`

5. The project is created in your personal space by default. Be aware that there is a limit of 10 projects in the personal space on the Michelin Gitlab.

## Part 3: Linking Local and Remote Repositories

1. We already have our local git repository, if you are not already in that directory, go to it.

2. We need to add the origin remote:
   `git remote add origin git@gitlab.com:<your username>/gitlab-training-name`

3. Verify the state of the repository with `git status`. Commit any modified files that we want to add to the repository (if any).

4. We can also check the repository log with `git log` (you may need to type `q` to return to the command line)

5. If everything is as we want it, we can push the repository with `git push -u origin --all`

   a. If we have any local tags, we can push these as well with `git push -u origin --tags`

## Part 4: Making Changes

1. The next step will be to make changes to someone else's project. For that, we'll need to pair up.


   a. To create the fork, we need to go to the partner's project page. The URL is generally `https://gitlab.com/<username>/gitlab-training-name`

   b. Click on the `Fork` button to create a copy of the repository to your personal projects ![Fork](img/fork.png)

3. We can clone the project locally.

   a. if you are in the project directory for `git-training`, then exit it with `cd ..`.

   b. Clone the forked repository: `git clone git@gitlab.com:<your-username>/gitlab-training-name` (You may want to make a subdirectory to avoid naming conflicts)

4. `cd` to the new project. You should be on the `master` branch.

5. Create and checkout a new branch.

6. Add a new line to the `trainees.txt` file.

7. Commit the changes.

8. Now we have the changes locally, we need to pus them remotely. We can do this with `git push -u origin <branch-name>`

**N.B.** The `-u` option is the same as the `--set-upstream` option. it's just shorter to write. It is also not strictly necessary, but it allows us to simply use `git push` on the next time we want to push changes.

## Part 5: Requesting a Merge

1. Now that we made a change, we want to merge it back to the original repository using a merge request. First, go to the gitlab page of the forked repository; it should be at `https://gitlab.com/<your-username>/gitlab-training-name`

2. Click on the `Merge Requests` tab and create a new merge request.

3. Verify that the source branch is from your repository, and the target branch is `master` in your partner's repository.

4. Fill in the fields, adding a short summary of the changes, a longer description if warranted, a link to the issue the merge would fix, etc.

5. Submit the new merge request.

## Part 6: Merging

1. Assuming your partner followed all the steps above, you should have a new merge request on your personal `gitlab-training` project page.

2. You can review the merge request by clicking on `Merge Requests` and then the link on the merge request on that page.

3. You can discuss the issue using comments and discussion threads. Comments are simply comments on the merge request, they require no validation and are generally informational in nature. Discussions are where major problems can be discussed. A new discussion will open a thread to which we can respond, and the project can be set such that ALL discussions most be resolved in order to merge the request.

4. The changes can be viewed as a diff (either side-by-side or inline). Red represents lines that have been removed, and green lines that have been added.

5. Review the changes and accept the merge request.

## Part 7: Pulling the new changes

1. Once we have merged the new code, we aren't quite done yet. Return to the `git-training` directory in the git bash prompt.

2. To retrieve the changes, we perform the `git pull` command.

3. The changes should now be available in the local repository.

![Merging Complete](img/mergecomplete.jpg)
