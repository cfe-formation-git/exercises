# Exercise 2: First Steps

## Part 1: Initializing a repository

We'll need to start somewhere. First we'll create a new directory and initialize a git repository inside.

1. Create a new folder called `git-training`

2. In the git bash window, go to the `git-training` directory that we just created.

3. Initialize a new git repository with `git init .`

## Part 2: Add files to the repository

Next we need something in the repository.

1. In the `git-training` directory, create a new file called `trainees.txt`

2. Add the following to the file:

```csv
Name, Project, Skill
"Adam Bertrand", "PRJ1", 5
"Aurelien Fleury-Bougé", "PRJ5", 4
"Jean-Paul", "DIR", 1
```

3. Verify the status of the working directory with `git status`. The new file should be `untracked`

4. Add the file to the repository with the following command: `git add trainees.txt`

5. Verify the status of the working directory once more with `git status`. Note the new status

6. Finally, commit the file with `git commit`
