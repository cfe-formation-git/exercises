# Exercise 4: Conflict Resolution 

While git branches are a powerful tool, conflicts can still arise. Let us start once again by adding a new feature

## Part 1: Adding a new feature

1. Create a new branch named `feature/modify-trainees`

2. If you haven't already done so, switch to the new branch.

3. Make a new modification to the `trainees.txt` file: Modify `Jean-Paul` to `Jean-François`

4. Commit the changes to the repository.

## Part 2: Remote modification

1. Checkout the `master` branch so simulate a remote modification

2. Make a new modification to the `trainees.txt` file: Modify `Jean-Paul` to `Jean-Philippe`

3. Commit the new change to simulate another teammate having modified the file.

## Part 3: Updating our feature

1. Checkout the `feature/modify-trainees` branch.

2. Update the branch with master. Since we are purely local, we can use a `rebase`: `git rebase master`

3. This creates a conflict! Resolve it using either a configured mergetool or a text editor.

---
Aside:

I personally prefer using [Visual Studio Code](https://code.visualstudio.com/) as a text editor. It is reasonably lightweight, extensible and fairly fast.

To configure it as a mergetool, add the following to your global git configuration:

```ini
[merge]
    tool = vscode
[mergetool "vscode"]
    cmd = code --wait $MERGED
```
---

4. The conflicted file should look like the following:

```csv
Name, Project, Skill
"Adam Bertrand", "PRJ1", 5
"Aurelien Fleury-Bougé", "PRJ5", 5
<<<<<<< HEAD
"Jean-Philippe", "DIR", 1
=======
"Jean-François", "DIR", 1
>>>>>>> Update trainee
"Paul", "CVP", 2
```

The top portion is the change on `master`. It is marked as the HEAD branch as we are rebasing on top of master. In a classic merge, the roles would be inverted.

The bottom portion of the conflict is the change we wish to make. To accept the change, we must remove all the lines except for those we wish to keep. The final result should be the file as we wish it to be.

If you are using Visual Studio Code, then the Accept Incoming Change button can be pressed to automatically delete the unnecessary lines.

The end result should be the following:

```csv
Name, Project, Skill
"Adam Bertrand", "PRJ1", 5
"Aurelien Fleury-Bougé", "PRJ5", 5
"Jean-François", "DIR", 1
"Paul", "CVP", 2
```

4. Once the conflict is resolved, save the file and mark the conflict as resolved using the `git add` command.

5. Continue the rebase with `git rebase --continue`

## Part 4: Commiting our changes to master

1. Checkout the `master` branch.

2. Merge the feature branch into `master` with `git merge`
