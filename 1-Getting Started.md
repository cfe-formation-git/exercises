# Exercise 1: Getting Started

## Part 1: Installing Git

The first step to using git is to install it.

1. Download the git installation binary from <https://git-scm.com/download/win>

2. Launch the downloaded installer

3. Leave the defaults, unless you want to change the default editor (This may be a good idea)

4. When asking for the HTTPS transport backend, check the native Windows Secure Channel Library

5. On the next page, select `Checkout as-is, commit unix-style line endings`

6. Leave the rest on the defaults

7. Install git.

## Part 2: Configuring Git

Git's default configurations are generally sane, but there are a few required configuration options to set.

1. Open a git bash shell (generally available from the start menu)

2. Configure the user name and email address

   a. `git config --global user.name "Your full name here"`

   b. `git config --global user.email "your email address here"`

3. Line endings are generally problematic. Windows line endings on unix machines will generally break things. To fix most issues, we can add the following configuration options to configure the line ending format:

   a. `git config --global core.autocrlf input`

   b. `git config --global core.eol lf`

This will cause line endings to be stored as `\n` in the repository, and checked out as committed on the local machine. Be sure to properly set the line endings when working on files to be executed on Unix type machines.

4. By default, `git pull` will attempt to merge the remote branch with the local branch. This can add unnecessary commits to the repository, add the following configuration options to configure the pull and fetch behaviour to be somewhat cleaner.

   a. `git config --global pull.rebase true`

   b. `git config --global fetch.prune true`

The first option will set the `pull` command to use `rebase` instead of `merge` to get updates. The second will delete the local references to remote branches that have been deleted. This will **not** delete local branches that have been removed remotely.
