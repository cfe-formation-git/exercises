# Exercise 3: Branching Out

Branching is one of the most powerful features in git. Mastering branches is essential for an efficient workflow.

## Part 1: Adding a new feature

1. Create a new branch named `feature/new-trainees`

2. If you haven't already done so, switch to the new branch.

3. Modify the `trainees.txt` file to add the following line:

```csv
Paul, CVP, 2
```

4. Commit the new file to the repository.

## Part 2: Remote modification

1. Checkout the `master` branch

2. Make a new modification to the `trainees.txt` file: Modify Aurelien's skill level to `5`

3. Commit the new change to simulate another teammate having modified the file.

## Part 3: Updating our feature

1. Checkout the `feature/new-trainees` branch.

2. Update the branch with master. Since we are purely local, we can use a `rebase`: `git rebase master`

## Part 4: Commiting our changes to master

1. Checkout the `master` branch.

2. Merge the feature branch into `master` with `git merge`
